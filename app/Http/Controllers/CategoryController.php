<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    function index(){
        $collection = Category::orderBy('id', 'desc')->get();
    
        return view('category.index', [
          'collection' => $collection
        ]);
    }

    function edit($id){
        $item = Category::find($id);
    
        if (!$item) {
          return abort(404);
        }
    
        return view('category.edit', [
          'item' => $item
        ]);
    }

    function store(Request $req){
        try {
          $validator = Validator::make($req->all(), [
            'title' => 'required',
          ]);
    
          if ($validator->fails()) {
            throw new InvalidArgumentException("Pemberitahuan : ".$validator->errors()->first(), 500);
          }
    
          $create = Category::create($req->only('title'));
    
          return redirect('/category');
    
        } catch (\Throwable $th) {
          return response()->json([
            'message' => $th->getMessage()
          ], 500);
        }
    }

    function update(Request $req){
        try {
          $validator = Validator::make($req->all(), [
            'id' => 'required',
            'title' => 'required',
          ]);
    
          if ($validator->fails()) {
            throw new InvalidArgumentException("Pemberitahuan : ".$validator->errors()->first(), 500);
          }
    
          $find = Category::find($req->id);

          if (!$find) {
            throw new InvalidArgumentException("Data tidak ditemukan!", 500);
          }

          $find->update($req->only('title'));
    
          return redirect('/category');
    
        } catch (\Throwable $th) {
          return response()->json([
            'message' => $th->getMessage()
          ], 500);
        }
    }
    
    function delete(Request $req){
        try {
            $find = Category::find($req->id);
            if (!$find) {
              throw new InvalidArgumentException("Data tidak ditemukan!", 500);
            }
            $find->delete();
            return redirect('/category');
        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}
