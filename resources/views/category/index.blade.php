<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Kategori</title>

  <link 
    rel="stylesheet" type="text/css" 
    href="{{asset('custom/custom.css?ls=25071038')}}" />
</head>
<body>

  <form class="card" action="{{url('category/store')}}" method="post">
    @csrf

    <x-partial.input
      label="Judul Kategori" placeholder="Masukan judul kategori"
      id="titleInput" name="title" />

    <button type="submit">Simpan</button>
  </form>

  <h4>Kategori :</h4>
  <table>
    <thead>
      <tr>
        <td>Judul Kategori</td>
        <td>Aksi</td>
      </tr>
    </thead>
    <tbody>
      @foreach ($collection as $item)
        <tr>
          <td>{{$item->title}}</td>
          <td>
            <a href="{{url('category/edit/' . $item->id)}}">Edit</a>
            
            <x-partial.delete url="category/delete" id="{{$item->id}}" />
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

</body>
</html>