TUGAS - 01 Agustus 2023 :

- Buatlah project baru laravel `laravel_mandiri`

- Buat migration table `user_address` dengan field 
  ```
    bigInteger(user_id), 
    string(provinsi), 
    string(kota), 
    string(kecamatan), 
    string(desa), 
    string(kampung), 
    string(detail_alamat)
  ```

- Buat migration table `user_account` dengan field
  ```
    bigInteger(user_id), 
    string(bank), 
    string(cabang), 
    string(account_number), 
    string(account_name),  
  ```

- Buat lah 2 MODEL pada laravel dengan nama `UserAccount`, `UserAddress`

- Isi `protected $table` dan `protected $fillable` pada masing masing model sesuai dengan migration table

- Buat lah CONTROLLER pada laravel dengan nama `UserController`

- Buat lah WEB ROUTE pada laravel dengan url `user, [UserController::class, index]`

- Buat lah fungsi INDEX pada CONSTROLLER UserController, lalu tambahkan `return view(user.index)`

- Buat lah FORM pada `view(user.index)` untuk menambahkan user 
  (name input nya bisa di lihat di MODEL User pada bagian <protected $fillable>)

- Buat table sederhana pada `view(user.index)` untuk menampilkan email user

- Buat database `laravel_mandiri`

- Ubah `DB_DATABASE` pada file `.env`

- Lakukan `php artisan migrate` pada terminal

TUGAS LANJUTAN : 

- Buat lah fungsi tambah `user`

- Buat lah fungsi edit `user`

- Buat lah fungsi delete `user`
